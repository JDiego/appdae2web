﻿using System.Net.Http;
using System.Net.Http.Headers;
using System;
using AppDAE2Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AppDAE2Web.Areas.Asistencia.Services
{
    public class FicSrvCatEdificiosDetails
    {
        //FIC: Cliente para ejecutar las web api
        HttpClient FicClient = new HttpClient();
        //FIC: CONSTRUCTOR
        public FicSrvCatEdificiosDetails()
        {
            //FIC: Inicializao el valor de sus direccion base
            FicClient.BaseAddress = new Uri("http://localhost:63324");
            FicClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<eva_cat_edificios>> FicGetItemCatEdificios(Int16 id)
        {
            HttpResponseMessage FicResponse = await FicClient.GetAsync($"api/FicGetListCatEdificios/{id}");
            if (FicResponse.IsSuccessStatusCode)
            {
                var FicRespuesta = await FicResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<eva_cat_edificios>>(FicRespuesta);
            }
            return new List<eva_cat_edificios>();
        }

    }
}
