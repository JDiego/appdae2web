﻿using AppDAE2Web.Areas.Asistencia.Services;
using Microsoft.AspNetCore.Mvc;
using AppDAE2Web.Models;
using System.Collections.Generic;
using System;

namespace AppDAE2Web.Areas.Asistencia.Controllers
{
    public class CatEdificiosController : Controller
    {
        public IActionResult FicViCatEdificiosList()
        {
            try
            {
                //FIC: Aqui se cre la instancia del servicio que tendra los metodos relacionados con la vista
                FicSrvCatEdificiosList FicServicio = new FicSrvCatEdificiosList();

                //Fic: Aq
                List<eva_cat_edificios> FicList = FicServicio.FicGetListCatEdificios().Result;
                ViewBag.Title = "Catalogo de edificios";

                return View(FicList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult FicViCatEdificiosDetails(Int16 id)
        {
            try
            {
                //FIC: Aqui se cre la instancia del servicio que tendra los metodos relacionados con la vista
                FicSrvCatEdificiosDetails FicServicio = new FicSrvCatEdificiosDetails();

                //Fic: Aqui
                List<eva_cat_edificios> FicItem = FicServicio.FicGetItemCatEdificios(id).Result;
                ViewBag.Title = "Item de edificio";
                return View(FicItem.ToArray()[0]);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult FicViEjemplo()
        {
            return View();
        }
    }
}